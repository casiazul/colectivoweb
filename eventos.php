<div class="span9" >
    <link rel="stylesheet" href="css/eventos.css">
    <link rel="stylesheet" href="css/bootstrap-image-gallery.min.css">
    
    <section id="eventos"  data-type="background" data-speed="-10">
        <div class="row-fluid">
            <div class="span11 offset1">
                <article><h1 class="frase">EVENTOS</h1></article>
                <div id="myCarousel" class="carousel slide">
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        <div class="active item">
                            <div class="miItem">
                                <img class="foto" src="img/eventos/fotos/ppal/DSC_0201.jpg"/>
                            </div>
                        </div>
                        <?php include ("fotos.php");?>
                    </div>
                    <!-- Carousel nav -->
                    <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                    <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span11 offset1">
                <div class="row-fluid evento">                  
<!-- JORNADA CULTURA LIBRE -->                   
                    <div class="span2">
                        <div>
                            <div id="gallery" data-toggle="modal-gallery" data-target="#modal-gallerya" >
                                <a href="img/eventos/fotos/jcl/04.JCL - 4.11.jpg" title="Jornada Cultura Libre" rel="gallery">
                                    <img src="img/eventos/jornada_cultura_libre.png" class="img-polaroid" id="jcl">
                                </a>
                                <?php
                                    $ruta="img/eventos/fotos";   
                                    $carpeta="jcl";
                                    $dirint = dir($ruta.'/'.$carpeta);
                                    while (($archivo = $dirint->read()) !== false){
                                        if (eregi("gif", $archivo) || eregi("jpg", $archivo) || eregi("png", $archivo)){
                                            echo '<a href="'.$ruta.'/'.$carpeta.'/'.$archivo.'" title="Jornada Cultura Libre" rel="gallery"></a>';
                                        }
                                    }
                                    $dirint->close();
                                ?>
                            </div>
                        </div>
                        <!-- controles galería de imagen -->
                        <div id="modal-gallery" class="modal modal-gallery hide fade" tabindex="-1">
                            <div class="modal-header">
                                <a class="close" data-dismiss="modal">&times;</a>
                                <h3 class="modal-title"></h3>
                            </div>
                            <div class="modal-body">
                                <div class="modal-image"></div>
                            </div>
                            <div class="modal-footer">
                                <a class="btn btn-trabajos modal-play modal-slideshow" data-slideshow="5000"><i class="icon-play"></i>
                                    <span>Presentación</span>
                                </a>
                                <a class="btn btn-trabajos modal-prev"><i class=" icon-chevron-left"></i>
                                    <span>Anterior</span>
                                </a>
                                <a class="btn btn-trabajos modal-next"><span>Siguiente</span>
                                    <i class="icon-chevron-right"></i>
                                </a>
                            </div>
                        </div>
                        <!--Fin controles de galería de imagen-->
                    </div>
                    <div class="span4 detalle">
                        <h4>Jornada de Cultura Libre</h4>
                        <p> Construcción del conocimiento libre y la cultura.</p>
                    </div>
<!-- JORNADA DERECHO DE AUTOR -->                    
                    <div class="span2">
                        <div>
                            <div id="gallery" data-toggle="modal-gallery" data-target="#modal-gallerya" >
                                <a href="img/eventos/fotos/jda/DSC_0202.jpg" title="Jornada Derecho de Autor" rel="gallery">                                 
                                    <img src="img/eventos/derecho_autor.png" class="img-polaroid" id="jda">
                                </a>
                                <?php
                                    $ruta="img/eventos/fotos";   
                                    $carpeta="jda";
                                    $dirint = dir($ruta.'/'.$carpeta);
                                    while (($archivo = $dirint->read()) !== false){
                                        if (eregi("gif", $archivo) || eregi("jpg", $archivo) || eregi("png", $archivo)){
                                            echo '<a href="'.$ruta.'/'.$carpeta.'/'.$archivo.'" title="Jornada Derecho de Autor" rel="gallery"></a>';
                                        }
                                    }
                                    $dirint->close();
                                ?>
                            </div>
                        </div>
                        <!-- controles galería de imagen -->
                        <div id="modal-gallerya" class="modal modal-gallery hide fade" tabindex="-1">
                            <div class="modal-header">
                                <a class="close" data-dismiss="modal">&times;</a>
                                <h3 class="modal-title"></h3>
                            </div>
                            <div class="modal-body">
                                <div class="modal-image"></div>
                            </div>
                            <div class="modal-footer">
                                <a class="btn btn-trabajos modal-play modal-slideshow" data-slideshow="5000"><i class="icon-play"></i>
                                    <span>Presentación</span>
                                </a>
                                <a class="btn btn-trabajos modal-prev"><i class=" icon-chevron-left"></i>
                                    <span>Anterior</span>
                                </a>
                                <a class="btn btn-trabajos modal-next"><span>Siguiente</span>
                                    <i class="icon-chevron-right"></i>
                                </a>
                            </div>
                        </div>
                        <!--Fin controles de galería de imagen-->
                    </div>     
                    <div class="span4 detalle">
                        <h4>Jornada Derecho de Autor</h4>
                        <p> Construcción del conocimiento libre y la cultura.</p>
                    </div>
                </div>
                <div class="row-fluid evento">
<!-- SOMA 2012 -->
                    <div class="span2">
                        <div>
                            <div id="gallery" data-toggle="modal-gallery" data-target="#modal-gallerya" >
                                <a href="img/eventos/fotos/soma2012/soma0.jpg" title="Soma 2012" rel="gallery">
                                    <img src="img/eventos/soma1.jpg" class="img-polaroid" id="soma">
                                </a>
                                <?php
                                    $ruta="img/eventos/fotos";   
                                    $carpeta="soma2012";
                                    $dirint = dir($ruta.'/'.$carpeta);
                                    while (($archivo = $dirint->read()) !== false){
                                        if (eregi("gif", $archivo) || eregi("jpg", $archivo) || eregi("png", $archivo)){
                                            echo '<a href="'.$ruta.'/'.$carpeta.'/'.$archivo.'" title="Soma 2012" rel="gallery"></a>';
                                        }
                                    }
                                    $dirint->close();
                                ?>
                            </div>
                        </div>
                        <!-- controles galería de imagen -->
                        <div id="modal-gallerya" class="modal modal-gallery hide fade" tabindex="-1">
                            <div class="modal-header">
                                <a class="close" data-dismiss="modal">&times;</a>
                                <h3 class="modal-title"></h3>
                            </div>
                            <div class="modal-body">
                                <div class="modal-image"></div>
                            </div>
                            <div class="modal-footer">
                                <a class="btn btn-trabajos modal-play modal-slideshow" data-slideshow="5000"><i class="icon-play"></i>
                                    <span>Presentación</span>
                                </a>
                                <a class="btn btn-trabajos modal-prev"><i class=" icon-chevron-left"></i>
                                    <span>Anterior</span>
                                </a>
                                <a class="btn btn-trabajos modal-next"><span>Siguiente</span>
                                    <i class="icon-chevron-right"></i>
                                </a>
                            </div>
                        </div>
                        <!--Fin controles de galería de imagen-->  
                    </div>
                    <div class="span4 detalle">
                        <h4>SOMA 2012</h4>
                        <p> Festival creativo organizado por Caja Negra.</p>
                    </div>
<!-- BIENAL 2012 -->     
                    <div class="span2">
                        <div>
                            <div id="gallery" data-toggle="modal-gallery" data-target="#modal-gallerya" >
                                <a href="img/eventos/fotos/bienal2012/DSC_0161.jpg" title="Bienal 2012" rel="gallery">
                                    <img src="img/eventos/bienal1.jpg" class="img-polaroid" id="bienal">
                                </a>
                                <?php
                                    $ruta="img/eventos/fotos";   
                                    $carpeta="bienal2012";
                                    $dirint = dir($ruta.'/'.$carpeta);
                                    while (($archivo = $dirint->read()) !== false){
                                        if (eregi("gif", $archivo) || eregi("jpg", $archivo) || eregi("png", $archivo)){
                                            echo '<a href="'.$ruta.'/'.$carpeta.'/'.$archivo.'" title="Bienal 2012" rel="gallery"></a>';
                                        }
                                    }
                                    $dirint->close();
                                ?>
                            </div>
                        </div>
                        <!-- controles galería de imagen -->
                        <div id="modal-gallerya" class="modal modal-gallery hide fade" tabindex="-1">
                            <div class="modal-header">
                                <a class="close" data-dismiss="modal">&times;</a>
                                <h3 class="modal-title"></h3>
                            </div>
                            <div class="modal-body">
                                <div class="modal-image"></div>
                            </div>
                            <div class="modal-footer">
                                <a class="btn btn-trabajos modal-play modal-slideshow" data-slideshow="5000"><i class="icon-play"></i>
                                    <span>Presentación</span>
                                </a>
                                <a class="btn btn-trabajos modal-prev"><i class=" icon-chevron-left"></i>
                                    <span>Anterior</span>
                                </a>
                                <a class="btn btn-trabajos modal-next"><span>Siguiente</span>
                                    <i class="icon-chevron-right"></i>
                                </a>
                            </div>
                        </div>
                        <!--Fin controles de galería de imagen-->
                    </div>
                    <div class="span4 detalle">
                        <h4>10 Bienal de Arte Joven</h4>
                        <p> Construcción del conocimiento libre y la cultura.</p>
                    </div>    
                </div>
                <div class="row-fluid evento">
<!-- LIBRE BUS 2012 -->
                    <div class="span2">
                        <div>
                            <div id="gallery" data-toggle="modal-gallery" data-target="#modal-gallerya" >
                                <a href="img/eventos/fotos/librebus2012/1.jpg" title="Libre Bus 2012" rel="gallery">
                                    <img src="img/eventos/librebus1.png" class="img-polaroid" id="lb">
                                </a>
                                <?php
                                    $ruta="img/eventos/fotos";   
                                    $carpeta="librebus2012";
                                    $dirint = dir($ruta.'/'.$carpeta);
                                    while (($archivo = $dirint->read()) !== false){
                                        if (eregi("gif", $archivo) || eregi("jpg", $archivo) || eregi("png", $archivo)){
                                            echo '<a href="'.$ruta.'/'.$carpeta.'/'.$archivo.'" title="Libre BUS 2012" rel="gallery"></a>';
                                        }
                                    }
                                    $dirint->close();
                                ?>
                            </div>
                        </div>
                        <!-- controles galería de imagen -->
                        <div id="modal-gallerya" class="modal modal-gallery hide fade" tabindex="-1">
                            <div class="modal-header">
                                <a class="close" data-dismiss="modal">&times;</a>
                                <h3 class="modal-title"></h3>
                            </div>
                            <div class="modal-body">
                                <div class="modal-image"></div>
                            </div>
                            <div class="modal-footer">
                                <a class="btn btn-trabajos modal-play modal-slideshow" data-slideshow="5000"><i class="icon-play"></i>
                                    <span>Presentación</span>
                                </a>
                                <a class="btn btn-trabajos modal-prev"><i class=" icon-chevron-left"></i>
                                    <span>Anterior</span>
                                </a>
                                <a class="btn btn-trabajos modal-next"><span>Siguiente</span>
                                    <i class="icon-chevron-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="span4 detalle">
                        <h4>LibreBUS cono sur</h4>
                        <p>4 países. Una veintena de librenautas a bordo. Mucho que hacer y compartir.</p>
                    </div>
<!-- FLISOL 2013 -->
                    <div class="span2">                        
                        <div>
                            <div id="gallery" data-toggle="modal-gallery" data-target="#modal-gallerya" >
                                <a href="img/eventos/fotos/flisol2013/DSC_0396.jpg" title="Flisol 2013" rel="gallery">
                                    <img src="img/eventos/flisol1.jpg" class="img-polaroid" id="fli">
                                </a>
                                <?php
                                    $ruta="img/eventos/fotos";   
                                    $carpeta="flisol2013";
                                    $dirint = dir($ruta.'/'.$carpeta);
                                    while (($archivo = $dirint->read()) !== false){
                                        if (eregi("gif", $archivo) || eregi("jpg", $archivo) || eregi("png", $archivo)){
                                            echo '<a href="'.$ruta.'/'.$carpeta.'/'.$archivo.'" title="Flisol 2013" rel="gallery"></a>';
                                        }
                                    }
                                    $dirint->close();
                                ?>
                            </div>
                        </div>
                        <!-- controles galería de imagen -->
                        <div id="modal-gallerya" class="modal modal-gallery hide fade" tabindex="-1">
                            <div class="modal-header">
                                <a class="close" data-dismiss="modal">&times;</a>
                                <h3 class="modal-title"></h3>
                            </div>
                            <div class="modal-body">
                                <div class="modal-image"></div>
                            </div>
                            <div class="modal-footer">
                                <a class="btn btn-trabajos modal-play modal-slideshow" data-slideshow="5000"><i class="icon-play"></i>
                                    <span>Presentación</span>
                                </a>
                                <a class="btn btn-trabajos modal-prev"><i class=" icon-chevron-left"></i>
                                    <span>Anterior</span>
                                </a>
                                <a class="btn btn-trabajos modal-next"><span>Siguiente</span>
                                    <i class="icon-chevron-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="span4 detalle">
                        <h4>Flisol 2013</h4>
                        <p> Festival Latinoamericano de Instalación de Software Libre.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="span3">
    <section id="fraseEvento" data-type="background" data-speed="-2">
        <div class="text">
            <p> Lee libremente:</br></br>
           		libros libres :: flacso.</a>
            	<a href="http://libroslibres.flacso.org.ar/"><i class="icon-book"></i></a></br></br>
            	Leer libros libres
            	<a href="http://leerlibroslibres.com.ar/libros/cultura-libre.html"><i class="icon-book"></i> </a></br></br>
            </p>
        </div>
    </section>
</div>