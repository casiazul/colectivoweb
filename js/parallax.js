$(document).ready(function(){
	// Cache the Window object
	$window = $(window);
                
   	$('section[data-type="background"]').each(function(){
    	var $bgobj = $(this); // assigning the object
                    
      	$(window).scroll(function() {
                    
			// Scroll the background at var speed
			// the yPos is a negative value because we're scrolling it UP!								
			var yPos = -($window.scrollTop() / $bgobj.data('speed')); 
			//var xPos = ($window.scrollTop()/$bgobj.data('speed')*2);
			
			// Put together our final background position
			var coords = '50% '+ yPos + 'px';
			//var coords = xPos + '%' + yPos + 'px';
			// Move the background
			$bgobj.css({ backgroundPosition: coords });
		
		}); // window scroll Ends

	 });	

	   	$('section[data-type="volador"]').each(function(){
    	var $bgobj = $(this); // assigning the object
                    
      	$(window).scroll(function() {
                    
			// Scroll the background at var speed
			// the yPos is a negative value because we're scrolling it UP!								
			var yPos = -($window.scrollTop() / $bgobj.data('speed')*1.3); 
			var xPos = ($window.scrollTop()/$bgobj.data('speed')*3.5);
			
			// Put together our final background position
			//var coords = '10% '+ yPos + 'px';
			var coords = -xPos + '%' + yPos + '%';
			// Move the background
			$bgobj.css({ backgroundPosition: coords });
		
		}); // window scroll Ends

	 });	

	   	$('div[data-type="text"]').each(function(){
    	var $bgobj = $(this); // assigning the object
                    
      	$(window).scroll(function() {
                    
			// Scroll the background at var speed
			// the yPos is a negative value because we're scrolling it UP!								
			var yPos = -($window.scrollTop() / $bgobj.data('speed')); 
			var xPos = ($window.scrollTop()/$bgobj.data('speed')*2.9);
			
			// Put together our final background position
			//var coords = '10% '+ yPos + 'px';
			var coords = -xPos + '%' + yPos + '%';
			// Move the background
			$bgobj.css({ backgroundPosition: coords });
		
		}); // window scroll Ends

	 });

   	$('div[data-type="background"]').each(function(){
    	var $bgobj = $(this); // assigning the object
                    
      	$(window).scroll(function() {
                    
			// Scroll the background at var speed
			// the yPos is a negative value because we're scrolling it UP!								
			var yPos = -($window.scrollTop() / $bgobj.data('speed')); 
			//var xPos = ($window.scrollTop()/$bgobj.data('speed')*2);
			
			// Put together our final background position
			var coords = '50% '+ yPos + 'px';
			//var coords = xPos + '%' + yPos + 'px';
			// Move the background
			$bgobj.css({ backgroundPosition: coords });
		
		}); // window scroll Ends

	 });
	 
	 $('#voladorSuperheroe').mouseenter(function(){
	 	//$('#superHeroe').addClass({"right": "+=750px"}, "slow");
	 	$('#superHeroe').fadeIn();
      	$('#superHeroe').animate({"right": "+=650px"}, 800);
      	$('#superHeroe').animate({"bottom": "-=20px"}, 600);
      	$('#superHeroe').animate({"right": "+=350px"}, 2000);
      	$('#superHeroe').fadeOut();

	 });


}); 
/* 
 * Create HTML5 elements for IE's sake
 */

document.createElement("article");
document.createElement("section");