<div class="span9 hacer">
    <link rel="stylesheet" href="css/queHacemos.css">
    <link rel="stylesheet" href="css/bootstrap-image-gallery.min.css">

 
    <section id="queHacemos"  data-type="background" data-speed="5">
        <article>
            <h2 class="frase">hacemos <BR>¡LO QUE MÁS NOS GUSTA!<!--</br> EMPRENDER LIBREMENTE--></h2>
        </article>
        <div class="span12 socios queHacemos">
            <div class="span4 offset4">
                <div class="row-fluid">
                    <h2 class="menuPalabra pull-left menuRed">redes</h2><br><br>
                </div>
                <div class="row-fluid">
                    <h2 class="menuPalabra pull-right menuMigracion">migraciones</h2><br><br>
                </div>
                <div class="row-fluid">
                    <h2 class="menuPalabra pull-left menuDisenio">diseño</h2><br><br>
                </div>
                <div class="row-fluid">
                    <h2 class="menuPalabra pull-right menuAsesoramiento">asesoramiento</h2><br><br>
                </div>
                <div class="row-fluid">
                    <h2 class="menuPalabra pull-right menuDesarrollo">desarrollo</h2><br><br>
                </div>
                <div>
                    <h2 class="menuPalabra pull-left menuCapacitacion">capacitaciones</h2><br>
                </div>
            </div>
        </div>                
        <div id="redes" class="categoria">
            <div class="span11 offset1">
                <div class="row-fluid">
                    <?php include ("extra/menuQueHacemos.php"); ?>
                </div>
                <div class="row">
                    <dl class="dl-horizontal">
                        <dt><br><br>RE<br>DES<br></dt>
                        <dd>
                            <ul class="unstyled">
                                <li><i class="icon-random"></i> Cableado Estructurado</li><br>
                                <li><i class="icon-globe"></i> Soluciones Inalámbricas </li><br>
                                <li><i class="icon-wrench"></i> Montaje de Servidores</li><br>  
                                <li><i class="icon-home"></i> Servicios de hosting</li><br>
                                <li><i class="icon-briefcase"></i> Asesoría en infraestructura de Redes</li><br>   
                            </ul>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
        <div id="migracion" class="categoria">
            <div class="span11 offset1">
                <div class="row-fluid">
                    <?php include ("extra/menuQueHacemos.php"); ?>
                </div>
                <div class="row">
                    <dl class="dl-horizontal">
                        <dt><br><br>MI<br>GRA<br>CIO<br>NES<br></dt>
                        <dd>
                            <ul class="unstyled">
                                <li><i class="icon-heart"></i> Migraciones a Software Libre</li><br>
                                <li><i class="icon-wrench"></i> Soporte técnico en Sistemas Operativos GNU/Linux</li><br>
                                <li><i class="icon-user"></i> Instalación y Configuración de programas libres</li><br> 
                                <li><i class="icon-ok"></i> Asesoría en Migraciones y Sistemas GNU/Linux</li><br>  
                            </ul>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
        <div id="disenio" class="categoria">
            <div class="span11 offset1">
                <div class="row-fluid">
                    <?php include ("extra/menuQueHacemos.php"); ?>
                </div>
                <div class="row">
                    <dl class="dl-horizontal">
                        <dt><br><br>DI<br>SE<br>ÑO<br></dt>
                        <dd>
                            <ul class="unstyled">
                                <li><i class="icon-star"></i> Identidad, imagen corporativa</li><br>
                                <li><i class="icon-folder-open"></i> Papeleria y papelería comercial <!--<small>[tarjetas, hojas, sobres, carpetas institucionales, etc]</small>--></li><br>
                                <li><i class="icon-picture"></i> Folleteria, tarjeteria y postales</li><br>
                                <li><i class="icon-book"></i> Catálogo de productos, menú, afiches</li><br>
                                <li><i class="icon-heart"></i> Diseño de merchandising</i></li><br>
                                <li><i class="icon-star"></i> Asesoramiento en Diseño Gráfico</li><br>   
                            </ul> 
                            <div>
                                <div id="gallery" data-toggle="modal-gallery" data-target="#modal-gallery" >
                                   <a href="img/disenio/prod6.png" title="Diseño Catálogo de Emprendedores" rel="gallery">
                                        <div class="btn btn-trabajos">ver trabajos</div>
                                    </a> 
                                    <a href="img/disenio/prod1.png" title="Diseño Tarjeta de 15 años" rel="gallery"></a>
                                    <a href="img/disenio/prod2.png" title="Diseño Cumpleaños 1 año" rel="gallery"></a>
                                    <a href="img/disenio/prod4.png" title="Diseño Catálogo Ferretería" rel="gallery"></a>
                                    <a href="img/disenio/prod5.png" title="Diseño Tarjetería" rel="gallery"></a>
                                    <a href="img/disenio/prod7.png" title="Diseño Logos" rel="gallery"></a>
                                    <a href="img/disenio/prod8.png" title="Diseño Logos" rel="gallery"></a>
                                    <a href="img/disenio/prod9.png" title="Diseño Logos" rel="gallery"></a>
                                    <a href="img/disenio/prod10.png" title="Diseño" rel="gallery"></a>
                                    <a href="img/disenio/prodWeb1.png" title="Diseño y Desarrollo Web" rel="gallery"></a>
                                    <a href="img/disenio/prodWeb2.png" title="Diseño y Desarrollo Web" rel="gallery"></a>
                                    <a href="img/disenio/prodWeb3.png" title="Diseño y Desarrollo Web" rel="gallery"></a>                                    
                                </div>
                            </div>
                            <!-- modal-gallery is the modal dialog used for the image gallery -->
                            <!-- controles galería de imagen -->
                            <div id="modal-gallery" class="modal modal-gallery hide fade" tabindex="-1">
                                <div class="modal-header">
                                    <a class="close" data-dismiss="modal">&times;</a>
                                    <h3 class="modal-title"></h3>
                                </div>
                                <div class="modal-body">
                                    <div class="modal-image"></div>
                                </div>
                                <div class="modal-footer">
                                    <a class="btn btn-trabajos modal-play modal-slideshow" data-slideshow="5000"><i class="icon-play icon-white"></i>
                                        <span>Presentación</span>
                                    </a>
                                    <a class="btn btn-trabajos modal-prev"><i class="icon-chevron-left icon-white"></i>
                                        <span>Anterior</span>
                                    </a>
                                    <a class="btn btn-trabajos modal-next"><span>Siguiente</span>
                                        <i class="icon-chevron-right icon-white"></i>
                                    </a>
                                </div>
                            </div>
                            <!--Fin controles de galería de imagen-->            
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
        <div id="asesoramiento" class="categoria">
            <div class="span11 offset1">
                <div class="row-fluid">
                    <?php include ("extra/menuQueHacemos.php"); ?>
                </div>
                <div class="row">
                    <dl class="dl-horizontal">
                        <dt><br><br>ASE</br>SORA</br>MIEN</br>TO</br></dt>
                        <dd>
                            <ul class="unstyled">
                                <li><i class="icon-user"></i> Asesoramiento en tecnologías</li><br>
                                <li><i class="icon-wrench"></i> Soluciones Ingenieriles</li><br>
                                <li><i class="icon-heart"></i> Consultoría en Software Libre</li><br> 
                            </ul>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
        <div id="desarrollo" class="categoria">                            
            <div class="span11 offset1">
                <div class="row-fluid">
                    <?php include ("extra/menuQueHacemos.php"); ?>
                </div>
                <div class="row">
                    <dl class="dl-horizontal">
                        <dt><br><br>DE<br>SA<br>RRO<br>LLO<br></dt>
                        <dd>
                            <ul class="unstyled">
                                <li><i class="icon-user"></i> Sistemas a medida</li><br>
                                <li><i class="icon-resize-full"></i> Desarrollo Ágil</li><br>
                                <li><i class="icon-wrench"></i> Soluciones Ingenieriles</li><br>
                                <li><i class="icon-heart"></i> Multiplataforma con código libre</li><br>
                                <li><i class="icon-thumbs-up"></i> Respaldados por la comunidad del Software Libre</li><br>
                                <li><i class="icon-bullhorn"></i> Asesoría en Desarrollo de Sistemas</li><br>
                            </ul>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
        <div id="capacitacion" class="categoria">
            <div class="span11 offset1">
                <div class="row-fluid">
                    <?php include ("extra/menuQueHacemos.php"); ?>
                </div>
                <div class="row">
                    <dl class="dl-horizontal">
                        <dt><br><br>CA<br>PA<br>CITA<br>CIÓN<br></dt>
                        <dd>
                            <ul class="unstyled">
                                <li><i class="icon-pencil"></i> Cursos de Libre Office</li><br>
                                <li><i class="icon-heart"></i> Capacitaciones en Software Libre</li><br>
                                <li><i class="icon-briefcase"></i> Capacitaciones para empresas</li><br>
                                <li><i class="icon-user"></i> Capacitaciones a particulares</li><br>
                                <li><i class="icon-eye-open"></i> Asesoramiento en Capacitaciones</li><br> 
                            </ul>
                        </dd>
                    </dl>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="span3">
    <section id="fraseQueHacemos" data-type="background" data-speed="-2">
        <div class="textQueHacemos">
            <p> 
                "El movimiento del Software Libre no se opone a ninguna persona, organización o programa 
                específico, sino a la práctica, muy difundada, de distrubiur software bajo condiciones 
                que limitan seriamente la libertad de los usuarios."<br><br>
                <small><em>Monopolios Artificiales sobre Bienes Intangibles</em></small>
            </p> 
            <a href="http://www.vialibre.org.ar/wp-content/uploads/2007/03/mabi.pdf"><i class="icon-download-alt"></i> mabi.pdf </a>
        </div>
    </section>
</div>

