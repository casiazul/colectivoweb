<script src="js/bootstrap-alert.js"></script>
<!--<script src="js/form.js"></script>-->
<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="js/jquery.validate.js"></script>
<link rel="stylesheet" href="css/foot.css">
    <div class="row-fluid footer">
        <div class="span7">
            <div class="row-fluid">
                <a href="mailto:contacto@colectivolibre.com.ar"><p class="contacto pull-right">contacto@colectivolibre.com.ar</p></a>
            </div>
            <div class="row-fluid">
                <div class="span12 iconos">
                    <hr class="linea">
                    <a href="https://www.facebook.com/colectivolibre?ref=ts&fref=ts"><img src="img/socialIcons/png/facebook.png" class="iconos"/></a>
                    <a href="https://twitter.com/ColectivoLibre"><img src="img/socialIcons/png/twitter.png" class="iconos"/></a>
                    <a href="http://www.linkedin.com/company/colectivo-libre"><img src="img/socialIcons/png/linkedin.png" class="iconos"/></a>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span5">
                    <img src="img/hola.png"  class="pull-right"/>
                </div>
                <div class="span7">
                	<!--
                    <form method="post" action="enviar.php">
                        <fieldset>
                            <label>nombre</label>
                            <input class="span12" type="text" placeholder="nombre" name="nombre">
                            <label>mail</label>
                            <input class="span12" type="text" placeholder="mail" name="mail">
                            <label>mensaje</label>
                            <textarea class="span12" rows="7" placeholder="Dejanos tu consulta" name="mensaje"></textarea>
                        	<br>
                        
	                    	<img src="foot/captcha/captcha.php" class="span2"/>
						    <div class="control-group" class="span12">
    							<div class="controls span10">
    								<div class="input-prepend span12">
    									<span class="add-on"><i class="icon-refresh"><a href="foot/captcha/captcha.php"></a></i></span>
										<input type="text" size="20" name="captcha" class="span11"/>
    								</div>
    							</div>
    						</div>
	                    	<button type="submit" class="btn btn-enviar">Enviar</button>
                    	</fieldset>
                    </form>
                    <?php
					if($_SESSION["mensaje"]){
						echo '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>
    						'.$_SESSION["mensaje"].'</div>';
					}
				    $_SESSION["mensaje"]=null;
                    ?>-->
              
                
                <!-- prueba 2 formulario-->
				    <form method="post" id="miFormulario" action="enviar.php">
				    	<label for="nombre">nombre</label>
                        <input class="required span12" id="nombre" name="nombre" placeholder="nombre" type="text">
					    
					    <label for="mail">mail</label>
                        <input class="required span12" id="mail" name="mail" placeholder="mail" type="email">
					    
					    <label for="mensaje">mensaje</label>
                        <textarea class="required span12" id="mensaje" name="mensaje" placeholder="Dejanos tu consulta" rows="7" type="text"></textarea>
					   	
					   	<img src="foot/captcha/captcha.php" class="span2"/>
							<div class="control-group" class="span12">
    							<div class="controls span10">
    								<div class="input-prepend span12">
    									<!--
    									<span class="add-on">
    										<i class="icon-refresh"><a href="foot/captcha/captcha.php"></a></i>
    									</span>-->
										<input class="required span12" name="captcha" size="20" type="text"/>
    								</div>
    							</div>
    						</div>
					    <button type="submit" class="btn btn-enviar">Enviar</button>
				    </form>                   
				     <!-- Mensajes si se envió o no el mail. -->
				    <?php
						if($_SESSION["mensaje_enviar"]){
							echo '<div class="alert alert-success">
									<button type="button" class="close" data-dismiss="alert">&times;
									</button>
    							'.$_SESSION["mensaje_enviar"].'</div>';
						}
				    	$_SESSION["mensaje_enviar"]=null;
                    ?>
                <!-- -->
            	</div>        
        	</div>
        </div>
        <div class="span5 logos">
            <img src="img/expresiva.png"/>
            </br></br>
            <img src="img/facttic.png" />
        </div>
    </div>
    <!-- Validación del formulario -->
	<script type="text/javascript">
		$().ready(function () {
			$("#miFormulario").validate({});
		});
	</script>
<!--</footer>--> 
