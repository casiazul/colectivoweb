<div class="span9">
    <link rel="stylesheet" href="img/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/culturaLibre.css">
    <section id="culturaLibre"  data-type="background" data-speed="3">
        <article><h2 class="frase">CULTURA LIBRE</h2></article>
        <div class="span12 logoCultura">
            <div class="span">
                <div class="row-fluid ">
                    <div class="span6 cultura">
                        <img id="izquierda_arriba" class="pull-right" src="img/culturaLibre/fotos1.png"/> 
                    </div>
                    <div class="span6 cultura" id="derecha">
                        <img id="derecha_arriba" class="pull-left" src="img/culturaLibre/manuales1.png"/>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span6 cultura">
                        <img id="izquierda_abajo" class="pull-right" src="img/culturaLibre/codigo1.png"/>
                    </div>
                    <div class="span6 cultura" id="derecha">
                        <img id="derecha_abajo" class="pull-left" src="img/culturaLibre/disenio1.png"/>
                    </div>
                </div>    
            </div>
        </div>
        <div class="row-fluid" id="descripciones">
            <div class="span11 offset1"></div>
<!-- MANUALES -->
            <div id="manualDescripcion" class="span11 offset1">
                <h2>Manuales y libros</h2>
                <div class="span11">
                    <div class="row-fluid">
                        <address>
                            <dl class="dl-horizontal">
                                <dt><a class="btn btn-medium" href="http://www.gnu.org/philosophy/fsfs/free_software.es.pdf"><i class="icon-download-alt"></i></a>                                </dt>
                                <dd>Software Libre para una Sociedad Libre<br><p>Richard Stallman</p></dd>
                            </dl> 
                        </address>    
                    </div>
                    <div class="row-fluid">
                        <address>
                            <dl class="dl-horizontal">
                                <dt><a class="btn btn-medium" href="http://vialibre.org.ar/arcopy.pdf"><i class="icon-download-alt"></i></a></dt>
                                <dd>Argentina Copyleft :: <br><p>Fundación Vía Libre</p></dd>
                            </dl> 
                        </address>    
                    </div>
                </div>
            </div>
<!-- CODIGO -->
            <div id="codigoDescripcion" class="span11 offset1">
                <h2>Programas Libres</h2>
                <div class="span11">
					<div class="row-fluid">
						<ul class="inline">
							<li class="lista"><img src="img/culturaLibre/icon-gimp.png"/><h4>Gimp <a href="http://www.gimp.org/"><i class="icon-download-alt"></i></a></h4><p>Editor de fotografías</p></li>
							<li class="lista"> <img src="img/culturaLibre/inkscape.ico"/> <h4>Inkscape <a href="http://inkscape.org/"><i class="icon-download-alt"></i></a></h4><p>Editor de imágenes</p></li>
							<li class="lista"><img src="img/culturaLibre/icon-mypaint.png"/><h4>MyPaint <a href="http://mypaint.intilinux.com/"><i class="icon-download-alt"></i></a></h4><p>Pintura</p>  </li>
							<li class="lista"><img src="img/culturaLibre/icon-aptana.jpeg"/><h4>Aptana <a href="http://www.aptana.com/"><i class="icon-download-alt"></i></a></h4><p>Entorno de desarrollo</p>  </li>
							<li class="lista"><img src="img/culturaLibre/firefox_icon.png"/><h4>Firefox <a href="http://www.mozilla.org/es-AR/firefox/fx/"><i class="icon-download-alt"></i></a></h4><p>Navegador Web</p> </li>
							<li class="lista"><img src="img/culturaLibre/libreoffice.png"/><h4>LibreOffice <a href="http://es.libreoffice.org/"><i class="icon-download-alt"></i></a></h4><p>Suite de ofimática</p> </li>
						</ul>
					</div>
                </div>
            </div>
<!-- DISEÑO -->
            <div id="disenioDescripcion" class="span11 offset1">
                <h2>Diseño</h2>
                <div class="span4">
                    <img src="img/culturaLibre/disenio/elefante.png" />
                    <p><a class="btn btn-medium" href="img/culturaLibre/disenio/elefante.ora"><i class="icon-download-alt"></i></a>[.ora para MyPaint]</p>                    
                </div>
                <div class="span6 offset1">
                    <div class="row-fluid">
                        <address>
                            <dl class="dl-horizontal peque">
                                <dt><a class="btn btn-medium" href="img/culturaLibre/disenio/matrizSerigrafia.svg"><i class="icon-download-alt"></i></a></dt>
                                <dd>Matríz para Serigrafía <i class="icon-tint"></i><br><p>Juli Weidmann</p></dd>
                            </dl> 
                        </address>    
                    </div>
                    <div class="row-fluid">
                        <address>
                            <dl class="dl-horizontal peque">
                                <dt><a class="btn btn-medium" href="img/culturaLibre/disenio/elMundo.svg"><i class="icon-download-alt"></i></a></dt>
                                <dd>Letering <i class="icon-text-height"></i><br><p>Juli Weidmann</p></dd>
                            </dl> 
                        </address>    
                    </div>
                    <div class="row-fluid">
                        <address>
                            <dl class="dl-horizontal peque">
                                <dt><a class="btn btn-medium" href="img/culturaLibre/disenio/logo_colectivoLibre.svg"><i class="icon-download-alt"></i></a></dt>
                                <dd>Intervenime <i class="icon-magic"></i><br><p>Juli Weidmann</p></dd>
                            </dl> 
                        </address>    
                    </div>
                </div>
            </div>
          
<!-- FOTOS -->
            <div id="fotosDescripcion" class="span11 offset1">
                <h2>fotos libres</h2>
                <div class="span5">
                    <img src="img/culturaLibre/fotos/portada_t.png" class="img-polaroid">
                    <img src="img/culturaLibre/fotos/postal4_escritprio.png" class="img-polaroid">
                </div>
                <div class="span5">
                    <img src="img/culturaLibre/fotos/postal3.png" class="img-polaroid">
                    <img src="img/culturaLibre/fotos/postal2.png" class="img-polaroid">
                </div>
                <div class="span12"><p><i class="icon-camera-retro"></i> Buscá más fotos en: </p><a href="http://www.flickr.com/juliweidmann/">http://www.flickr.com/juliweidmann/</a>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="span3">
    <section id="fraseCulturaLibre" data-type="background" data-speed="-2">
        <div class="textCulturaLibre">
            <p> "al hablar de dinero público, precisamente, se hace del todo necesario considerar 
                fórmulas de acceso abierto, de circulación libre del conocimiento, en lugar de modos 
                restrictivos de gestión de la cultura. Si en el mercado libre, copyleft puede 
                plantearse como «una» opción, en lo público, copyleft debería constituirse como 
                «la» opción.”(…)<br><br>
                <small><em>Lawrence Lessig</em></small></p>  
            <a href="http://www.derechosdigitales.org/culturalibre/cultura_libre.pdf"><i class="icon-download-alt"></i> derechosdigitales.org </a>
        </div>
    </section>
</div>