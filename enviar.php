<?php
$nombre = $_POST['nombre'];
$mail = $_POST['mail'];

$header = 'From: ' . $mail . " \r\n";
$header .= "X-Mailer: PHP/" . phpversion() . " \r\n";
$header .= "Mime-Version: 1.0 \r\n";
$header .= "Content-Type: text/plain";

$mensaje = "Este mensaje fue enviado por: " . $nombre  . " \r\n";
$mensaje .= "Su e-mail es: " . $mail . " \r\n";
$mensaje .= "Mensaje: " . $_POST['mensaje'] . " \r\n";
$mensaje .= "Enviado el: " . date('d/m/Y', time());

$para = 'contacto@colectivolibre.com.ar';
$asunto = 'Consulta desde colectivolibre.com.ar de'.$mail;

session_start();
$_SESSION["mensaje_enviar"] =null;

if(strtoupper($_REQUEST["captcha"]) == $_SESSION["captcha"]){
	// REMPLAZO EL CAPTCHA USADO POR UN TEXTO LARGO PARA EVITAR QUE SE VUELVA A INTENTAR
	$_SESSION["captcha"] = md5(rand()*time());
	// INSERTA EL CÓDIGO EXITOSO AQUI
	if (mail($para, $asunto, $mensaje)){
		header('Location: http://www.colectivolibre.com.ar/#contacto');
		$_SESSION["mensaje_enviar"] = 'gracias por comunicarte con nosotros :)';
	} else {
		$_SESSION["mensaje_enviar"] = 'Error al enviar el mail. intenta nuevamente :/';
	}
} else {
		$_SESSION["captcha"] = md5(rand()*time());
		header('Location: http://www.colectivolibre.com.ar/#contacto');
		$_SESSION["mensaje_enviar"] = 'Intenta escribir nuevamente el captcha :|';
	}
?>