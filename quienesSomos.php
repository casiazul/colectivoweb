<div class="span9">
    <link rel="stylesheet" href="css/quienesSomos.css">
    <section id="quienesSomos" data-type="background" data-speed="2">
        <article>
            <h2 class="frase">somos </br>SISTEMAS + ELECTRÓNICA + DISEÑO =><br> ¡HAY EQUIPO!</h2>
        </article>
        <div class="span12 socios">        
            <div class="row-fluid socio">
                <div class="span2 offset1">
                    <img src="img/socios/juli.png"/>
                </div>                  
                <div class="span9">
                    <h2 class="nombre">· Juli Weidmann</h2>
                    <p>
                        Ella hace que todo se vea lindo, aunque el resto de la Coope preferiría un cuadrado negro sobre el fondo blanco, 
                        ella hace que todo sea de color. Estudió Licenciatura en Diseño de Comunicación Visual en la universidad Nacional 
                        del Litoral, FADU, hoy está desarrollando su tesina de Carrera. Estudió Profesorado de Artes Visuales en la Universidad 
                        Autónoma de Entre Ríos, FHAyCSS. y asiste a cuanto taller se cruza: Diseño Editorial, Gráfica Digital, Análisis Fílmico, 
                        Fotografía. Hoy está incursionando en Serigrafía. Es la Diseñadora Libre. 
                    </p>
                </div>
            </div>
            <div class="row-fluid socio">
                <div class="span2 offset1">
                    <img src="img/socios/ale.png"/>
                </div>
                <div class="span9">
                    <h2 class="nombre">· Ale Prieto</h2>
                    <p>
                        Le falta el proyecto para recibirse de Ingeniero en Sistemas de Información en la Universidad Tecnológica Nacional, Facultad 
                        Regional Santa Fe. Perfeccionándose en redes, está cursando el último semestre de CCNA. Fue becario en el Laboratorio de 
                        Conectividad durante varios años, donde dió sus primeros pasos en Redes, Streaming y Virtualización. 
                        Hoy se desempeña como líder de proyectos en el área de desarrollo de sistemas dentro de Colectivo Libre, innovando constantemente. 
                        Una frase: “Mejora continua...”
                    </p>
                </div>
            </div>
            <div class="row-fluid socio">
                <div class="span2 offset1">
                    <img src="img/socios/chiquito.png"/>
                </div>
                <div class="span9">
                    <h2 class="nombre">· Chiquito Mendoza</h2>
                    <p>
                        Estudiando Ingeniería en Sistemas de Información en la UTN, FRSF. Fué becario en el Laboratorio de Conectividad de la facu,
                        donde se especializó en Redes y fue el precursor en Clientes Delgados. El referente nuestro en Tcos.
                        Construyó los cimientos de lo que hoy es Colectivo Libre, y luchó para que este sueño de muchos sea realidad.
                        Es el que pone la cara y va al frente. Defensor del Software Libre.
                    </p>
                </div>
            </div>
            <div class="row-fluid socio">
                <div class="span2 offset1">
                    <img src="img/socios/maxi.png"/>
                </div>
                <div class="span9">
                    <h2 class="nombre">· Maxi Rossier</h2>
                    <p>
                        Estudiante de Ingeniería en Sistemas de Información en UTN-FRSF. Experiencia principalmente en el área de redes. 
                        También trabaja en las áreas de Capacitaciones y Migración.
                    </p>
                </div>
            </div>
            <div class="row-fluid socio">
                <div class="span2 offset1">
                    <img src="img/socios/cele.png"/>
                </div>
                <div class="span9">
                    <h2 class="nombre">· Cele Weidmann</h2>
                    <p>
                        Estudiante de Ingeniería en Sistemas de Información en UTN-FRSF. 
                        Fue becario en el Laboratorio de Conectividad durante varios años, donde dió sus primeros pasos en Redes, 
                        Streaming y Virtualización. Realizó cursos de CCNA y Linux. Experiencia en el área de infraestructura de redes.  
                        Desarrollo de Sitios Web.  
                    </p>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="span3">
    <section id="fraseQuienesSomos" data-type="background" data-speed="-2">
        <div class="textQuienesSomos">
            <p>
                "En todo caso, la gran cantidad de desarrolladores de software libre, 
                 que deliberadamente renuncian a imponer condiciones restrictivas a la distribución, 
                 estudio y confección de obras derivadas de sus programas no sólo desmiente de plano 
                 que el copyright sea necesario para el desarrollo de la disciplina, sino que 
                 demuestra, por construcción, que un ambiente de cooperación y colaboración es mucho 
                 más fértil que uno de aislamiento y restricción."<br><br>
                 <small><em>Argentina Copyleft</em></small>
            </p>
            <a href="http://vialibre.org.ar/arcopy.pdf"><i class="icon-download-alt"></i> vialibre.org.ar/arcopy </a>  
        </div>
    </section>
</div>
