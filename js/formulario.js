$(function(){   
	$("#form").validate({ 
		rules: { 
			name: { required: true, minlength: 3 }, 
			email: { required: true, email: true }, 
			message: { required: true } 
			}, 
		messages: { 
			name: { required: 'Este campo es obligatorio', minlength: 'La longitud mínima: 3' }, 
			email: 'No válida dirección de correo electrónico', 
			message: { required: 'Este campo es obligatorio' } 
			}, 
		success: function(label) { 
					label.html('OK').removeClass('error').addClass('ok'); 
					setTimeout(function(){ 
								label.fadeOut(500); 
								}, 
					2000) 
					} 
		});   
});