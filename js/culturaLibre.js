$(document).on('ready', function() {
	//Fotos
	$('#izquierda_arriba').hover(
		function(){
			(this).src='img/culturaLibre/fotos2.png';		
		},
		function() {
			(this).src='img/culturaLibre/fotos1.png';	  
		}
	);
	$('#izquierda_arriba').on('click', function() {
		$('.cultura').animate({"height": "35px"})
		$('#derecha_arriba').animate({"width": "35px"}, "fast");
		$('#izquierda_arriba').animate({"width": "35px"}, "fast");
		$('#derecha_abajo').animate({"width": "35px"}, "fast");
		$('#izquierda_abajo').animate({"width": "35px"}, "fast");
		$('#manualDescripcion').hide();
		$('#codigoDescripcion').hide();
		$('#disenioDescripcion').hide();
		$('#fotosDescripcion').show();
	});
	//Manual
	$('#derecha_arriba').hover(
		function(){
			(this).src='img/culturaLibre/manuales2.png';		
		},
		function() {
			(this).src='img/culturaLibre/manuales1.png';
		}
	);
	$('#derecha_arriba').on('click', function() {
		$('.cultura').animate({"height": "35px"})
		$('#derecha_arriba').animate({"width": "35px"}, "fast");
		$('#izquierda_arriba').animate({"width": "35px"}, "fast");
		$('#derecha_abajo').animate({"width": "35px"}, "fast");
		$('#izquierda_abajo').animate({"width": "35px"}, "fast");
		$('#fotosDescripcion').hide();
		$('#codigoDescripcion').hide();
		$('#disenioDescripcion').hide();
		$('#manualDescripcion').show();
	});
	//Codigo
	$('#izquierda_abajo').hover(
		function(){
			(this).src='img/culturaLibre/codigo2.png';		
		},
		function() {
			(this).src='img/culturaLibre/codigo1.png';	  
	});
	$('#izquierda_abajo').on('click', function() {
		$('.cultura').animate({"height": "35px"})
		$('#derecha_arriba').animate({"width": "35px"}, "fast");
		$('#izquierda_arriba').animate({"width": "35px"}, "fast");
		$('#derecha_abajo').animate({"width": "35px"}, "fast");
		$('#izquierda_abajo').animate({"width": "35px"}, "fast");
		$('#fotosDescripcion').hide();
		$('#manualDescripcion').hide();
		$('#disenioDescripcion').hide();
		$('#codigoDescripcion').show();
	});
	//Diseñofotos
	$('#derecha_abajo').hover(
		function(){
			(this).src='img/culturaLibre/disenio2.png';		
		},
		function() {
			(this).src='img/culturaLibre/disenio1.png';	  
	});
	$('#derecha_abajo').on('click', function() {
		$('.cultura').animate({"height": "35px"})
		$('#derecha_arriba').animate({"width": "35px"}, "fast");
		$('#izquierda_arriba').animate({"width": "35px"}, "fast");
		$('#derecha_abajo').animate({"width": "35px"}, "fast");
		$('#izquierda_abajo').animate({"width": "35px"}, "fast");
		$('#fotosDescripcion').hide();
		$('#manualDescripcion').hide();
		$('#codigoDescripcion').hide();
		$('#disenioDescripcion').show();
	});

});

