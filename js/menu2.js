$('li').on('click', function() {
    $(this).parent().parent().find('.active').removeClass('active');
    $(this).parent().addClass('active');
});

$('#botonInicio').hover(
	function(){ (this).src='img/iconos/inicio2.png';},
	function(){(this).src='img/iconos/inicio1.png';	  
});
$('#botonQuienesSomos').hover(
	function(){(this).src='img/iconos/quienesSomos2.png';},
	function(){(this).src='img/iconos/quienesSomos1.png';	  
});
$('#botonQueHacemos').hover(
	function(){(this).src='img/iconos/queHacemos2.png';},
	function(){(this).src='img/iconos/queHacemos1.png';	  
});
$('#botonEventos').hover(
	function(){ (this).src='img/iconos/eventos2.png';},
	function(){(this).src='img/iconos/eventos1.png';	  
});
$('#botonCulturaLibre').hover(
	function(){(this).src='img/iconos/culturaLibre2.png';},
	function(){(this).src='img/iconos/culturaLibre1.png';	  
});
$('#botonContacto').hover(
	function(){(this).src='img/iconos/contacto2.png';},
	function(){(this).src='img/iconos/contacto1.png';	  
});