<?php
session_start();
?>
<!DOCTYPE html>
<head>
    <?php include ("head/head.php"); ?>
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <link rel="stylesheet" href="css/comun.css">
    <link rel="stylesheet" href="css/parallax.css">    
    <link rel="stylesheet" href="css/inicio.css">
</head>
<body>
<div class="container">
   
    <div class="row-fluid">
        <div class="span12">        
            <?php include ("top/top.php"); ?>          
        </div>
    </div>
   
    <div class="row-fluid">
        <ul class="unstyled affix menuBotones">     
            <li class="nav-item botones">
                <a href="#" class="nav_a_parallax">
                    <img src="img/iconos/inicio1.png" id="botonInicio"/>
                </a>
            </li>
            <li class="nav-item botones">
                <a href="#quienesSomos" class="nav_a_parallax">
                    <img src="img/iconos/quienesSomos1.png"id="botonQuienesSomos"/>
                </a>
            </li>
            <li class="nav-item botones">
                <a href="#queHacemos" class="nav_a_parallax">
                    <img src="img/iconos/queHacemos1.png" id="botonQueHacemos"/>
                </a>
            </li>
            <li class="nav-item botones">
                <a href="#culturaLibre" class="nav_a_parallax">
                    <img src="img/iconos/culturaLibre1.png" id="botonCulturaLibre"/>
                </a>
            </li>
            <li class="nav-item botones">
                <a href="#eventos" class="nav_a_parallax">
                    <img src="img/iconos/eventos1.png" id="botonEventos"/>
                </a>
            </li>
            <li class="nav-item botones">
                <a href="#contacto" class="nav_a_parallax">
                    <img src="img/iconos/contacto1.png" id="botonContacto"/>
                </a>
            </li>
        </ul>
    </div>
      
    <div class="row-fluid content_parallax fondo">
        <div class="row-fluid">
            <?php include ("inicio.php")?>
        </div>
        <div class="row-fluid">
            <?php include ('quienesSomos.php')?>
        </div>
        <div class="row-fluid">
            <section id="volador"  data-type="volador" data-speed="7">
            </section>    
        </div>
        <div class="row-fluid">
            <?php include ('queHacemos.php')?>
        </div>
        <div class="row-fluid">
            <section id="voladorSuperheroe"  data-type="volador" data-speed="7">
                <img src="img/volador.gif" id="superHeroe" class="pull-right" />
            </section>     
        </div>
        <div class="row-fluid">
           <?php include ("culturaLibre.php")?>
        </div>
        <div class="row-fluid">
           <?php include ("eventos.php")?>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <section id="contacto">
                    <?php include ("foot/foot.php"); ?>
                </section>                
            </div> 
        </div>
    </div>
</body> 
<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>-->
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="js/parallax.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/queHacemos.js"></script>
<script src="js/load-image.min.js"></script>
<script src="js/bootstrap-image-gallery.js"></script>
<script src="js/culturaLibre.js"></script>
<!--<script src="js/eventos.js"></script>-->
<script src="js/menu2.js"></script>
<script src="js/jquery.validate.js"></script>
