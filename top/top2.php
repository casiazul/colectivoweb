<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="js/menu2.js"></script>
<link rel="stylesheet" href="css/top.css">

<div class="row superior">
    <p>· Santa Fe·Argentina | 2013 ·</p>
</div>
<div class="row top">
    <div class="span3">
        <img src="img/logo.png" class="logo pull-right">
    </div>
    <div class="span9">
        <img src="img/logo_blanco_texto.png"/>
        <!--
        <h1>Colectivo Libre</h1>
        <h4>Cooperativa de Soluciones Integrales TICs</h4>-->
    </div>
</div>
<div class="row menu">
    <div class="navbar">
        <div class="navbar-inner ">
            <a class="brand" href="#"><img src="img/elefante_blanco.png" class="elefanteBlanco" /></a>
            <ul class="nav">
                <li class=""><a href="quienesSomos.php"><i class="rojo">|</i> quienes somos <i class="rojo">|</i></a></li>
                <li class=""><a href="queHacemos.php"><i class="verde">|</i> que hacemos <i class="verde">|</i></a></li>
                <li class=""><a href="#"><i class="azul">|</i> cultura libre <i class="azul">|</i></a></li>
                <li class=""><a href="#"><i class="violeta">|</i> eventos <i class="violeta">|</i></a></li>
            </ul>
        </div>
        <hr class="lineaAmarilla">
    </div>
</div>