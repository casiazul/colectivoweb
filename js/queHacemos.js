$(document).on('ready', function() {
//	$('.queHacemos').on('click', function() {
//		$(this).hide();
//		$('.queHacemos').fadeOut(4600, "linear");
//		$('#redes').fadeIn();
//	});
//	$('#vuelta').on('click', function() {
//		$('#redes').hide();
//		$('.queHacemos').show();
//	});
	
	$('.menuRed').on('click', function() {
		$('.queHacemos').hide();
		$('.categoria').hide();
		$('#redes').fadeIn();
	});
	$('.menuPalabra').hover( 
		//function(){$(this).css("color","yellow");},
		function(){
			$(this).css("color","#99BD46");
			$(this).css("text-shadow","2px 2px 3px #483737");},
		function(){
			$(this).css("color","#333333");
			$(this).css("text-shadow","none");
		
	});
	
	$('.menuMigracion').on('click', function() {
		$('.queHacemos').hide();
		$('.categoria').hide();
		$('#migracion').fadeIn();
	});
	$('.menuAsesoramiento').on('click', function() {
		$('.queHacemos').hide();
		$('.categoria').hide();
		$('#asesoramiento').fadeIn();
	});
	$('.menuElectronica').on('click', function() {
		$('.queHacemos').hide();
		$('.categoria').hide();
		$('#electronica').fadeIn();
	});
	$('.menuDisenio').on('click', function() {
		$('.queHacemos').hide();
		$('.categoria').hide();
		$('#disenio').fadeIn();
	});
	$('.menuCapacitacion').on('click', function() {
		$('.queHacemos').hide();
		$('.categoria').hide();
		$('#capacitacion').fadeIn();
	});
	$('.menuDesarrollo').on('click', function() {
		$('.queHacemos').hide();
		$('.categoria').hide();
		$('#desarrollo').fadeIn();
	});
	$('#vueltaM').on('click', function() {
		$('#migracion').hide();
		$('.queHacemos').show();
	});
	$('#vueltaR').on('click', function() {
		$('#redes').hide();
		$('.queHacemos').show();
	});
	$('#vueltaA').on('click', function() {
		$('#asesoramiento').hide();
		$('.queHacemos').show();
	});
	$('#vueltaD').on('click', function() {
		$('#disenio').hide();
		$('.queHacemos').show();
	});
	$('#vueltaDe').on('click', function() {
		$('#desarrollo').hide();
		$('.queHacemos').show();
	});
	$('#vueltaE').on('click', function() {
		$('#electronica').hide();
		$('.queHacemos').show();
	});
	$('#vueltaC').on('click', function() {
		$('#capacitacion').hide();
		$('.queHacemos').show();
	});
	// Cuando sale de la section queHacemos vuelve al menu principal. 
	//$('#queHacemos').mouseleave(function() {
	//	$('.categoria').hide();
	//	$('.queHacemos').show();
	//});	
	// Cuando con el mouse entra a la section culturaLibre o QuienesSomos vuelve al menu principal de queHacemos.
	$('#culturaLibre').mouseenter(function() {
		$('.categoria').hide();
		$('.queHacemos').show();
	});
	$('#quienesSomos').mouseenter(function() {
		$('.categoria').hide();
		$('.queHacemos').show();
	});
});

